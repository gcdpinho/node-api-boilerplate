# Node API boilerplate by Gustavo Pinho (nave.rs)

Things to do before run the project:

- Create database (by follow the commands):
  - CREATE USER `user` WITH PASSWORD `password`
  - CREATE DATABASE `database`
  - GRANT ALL PRIVILEGES ON DATABASE `database` to `user`
- Change knexfile.js (development) to thats informations
- Change name value of .env.example to .env and set the key SECRET to any value you wish
- Install knex global: `npm install -g knex`
- Run migrations: `knex migrate:latest`
- Run seeds: `knex seed:run`
- Run tests: `npm test`

If thats commands worked, your boilerplate api is ready.
Have fun!

