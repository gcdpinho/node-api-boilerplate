import Router from 'koa-router'

import UsersController from 'controllers/users-controller'
import UsersValidate from 'validators/users-schema'

const router = new Router()

router.get('/users', UsersController.index)
router.get('/users/:id', UsersController.show)
router.get('/me', UsersController.me)

router.post('/users/signup', UsersValidate.create(), UsersController.create)
router.post('/users/login', UsersController.login)

router.put('/users/:id', UsersValidate.update(), UsersController.update)

router.delete('/users/:id', UsersController.destroy)

export default router.routes()
