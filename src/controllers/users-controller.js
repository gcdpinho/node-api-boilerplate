import bcrypt from 'bcryptjs'

import User from 'models/User'

import { Unauthorized, encryptPassword, generateJWTToken } from 'helpers'

const UsersController = {
  login: async ctx => {
    const { body } = ctx.request

    const user = await new User({ email: body.email }).fetch().catch(() => {
      throw Unauthorized('Unauthorized, User not found')
    })

    const isValid = await bcrypt.compare(
      body.password,
      user.attributes.password
    )

    if (!isValid) {
      throw Unauthorized('Unauthorized, password is invalid')
    }

    const parsedUser = user.toJSON()

    return {
      ...parsedUser,
      token: generateJWTToken({ id: parsedUser.id, role: parsedUser.role })
    }
  },

  index: () => new User().fetchAll(),

  show: ctx => new User({ id: ctx.params.id }).fetch(),

  me: ctx => new User({ id: ctx.state.user.id }).fetch(),

  create: async ctx => {
    const { body } = ctx.request

    return new User().save({
      name: body.name,
      email: body.email,
      password: await encryptPassword(body.password),
      role: body.role
    })
  },

  update: async ctx => {
    const { body } = ctx.request

    return new User({ id: ctx.params.id }).save(
      {
        name: body.name,
        email: body.email,
        password: await encryptPassword(body.password),
        role: body.role
      },
      { method: 'update' }
    )
  },

  destroy: ctx => new User({ id: ctx.params.id }).destroy()
}

export default UsersController
